<?php

namespace Sinta\Laravel\Addons\Events;


use Sinta\Laravel\Addons\Environment;

class AddonWorldCreated
{
    public $world;

    public function __construct(Environment $world)
    {
        $this->world = $world;
    }
}