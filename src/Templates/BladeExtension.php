<?php

namespace Sinta\Laravel\Addons;

class BladeExtension
{
    public static function comment()
    {
        return function ($value) {
            $pattern = sprintf('/%s((.|\s)*?)%s/', '{#', '#}');

            return preg_replace($pattern, '<?php /*$1*/ ?>', $value);
        };
    }

    public static function script()
    {
        return function ($value) {
            $pattern = sprintf('/%s((.|\s)*?)%s/', '{@', '@}');

            return preg_replace($pattern, '<?php $1; ?>', $value);
        };
    }
}