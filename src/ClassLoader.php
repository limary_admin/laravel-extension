<?php

namespace Sinta\Laravel\Addons;

use Sinta\Laravel\Addons\Environment as AddonEnvironment;

class ClassLoader
{

    protected $env;
    protected $addons;

    protected static $instance;


    public static function register(AddonEnvironment $env, $addons)
    {
        static::$instance = new static($env, $addons);
        spl_autoload_register([static::$instance, 'load'], true, false);
    }


    public static function unregister()
    {
        if (static::$instance) {
            spl_autoload_unregister([static::$instance, 'load']);
        }
    }

    public function __construct(AddonEnvironment $env, array $addons)
    {
        $this->env = $env;
        $this->addons = $addons;
    }


    public function load($className)
    {
        foreach ($this->addons as $addon) {
            $namespace = $addon->phpNamespace();
            $namespacePrefix = $namespace ? $namespace.'\\' : '';
            if (!starts_with($className, $namespacePrefix)) {
                continue;
            }
            $relativeClassName = substr($className, strlen($namespacePrefix));
            $relativePath = $this->env->classToPath($relativeClassName);
            foreach ($addon->config('addon.directories') as $directory) {
                $path = $addon->path($directory.'/'.$relativePath);
                if (file_exists($path)) {
                    require_once $path;
                    return true;
                }
            }
        }
        return false;
    }
}