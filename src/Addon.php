<?php

namespace Sinta\Laravel\Addons;

use RuntimeException;

use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Foundation\Application as ApplicationContract;


class Addon
{
    /**
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    protected $name;

    /**
     * @var \Illuminate\Contracts\Config\Repository
     */
    protected $config;


    public static function create($app, $name, $path)
    {
        //$pathComponents = explode('/',$path);
        $config = static::loadAddonConfig($path, $name);
        return new static($app, $name, $path, $config);
    }


    protected static function loadAddonConfig($path, $name)
    {
        if (file_exists($path.'/addon.php')) {
            $config = require $path.'/addon.php';
        } else {
            throw new RuntimeException("No such config file for addon '$name', need 'addon.php'.");
        }
        $version = array_get($config, 'version', 5);
        if ($version != 5) {
            throw new RuntimeException($version.': Illigal addon version.');
        }
        return $config;
    }

    public function __construct($app, $name, $path, array $config)
    {
        $this->app = $app;
        $this->name = $name;
        $this->path = $path;
        $this->config = new ConfigRepository();
        $this->config->set('addon', $config);
    }

    public function name()
    {
        return $this->name;
    }

    public function path($path = null)
    {
        if (func_num_args() == 0) {
            return $this->path;
        } else {
            return $this->path.'/'.$path;
        }
    }


    public function relativePath(ApplicationContract $app)
    {
        return substr($this->path, strlen($app->basePath()) + 1);
    }

    public function version()
    {
        return $this->config('addon.version', 5);
    }


    public function phpNamespace()
    {
        return trim($this->config('addon.namespace', ''), '\\');
    }

    public function config($key, $default = null)
    {
        return $this->config->get($key, $default);
    }


    public function setConfig($key, $value)
    {
        $this->config->set($key, $value);
    }

    public function transName($resource)
    {
        return $this->name.'::'.$resource;
    }


    public function trans()
    {
        $args = func_get_args();
        $args[0] = $this->transName($args[0]);
        return call_user_func_array([$this->app['translator'], 'trans'], $args);
    }

    public function transChoice()
    {
        $args = func_get_args();
        $args[0] = $this->transName($args[0]);
        return call_user_func_array([$this->app['translator'], 'transChoice'], $args);
    }

    public function viewName($resource)
    {
        return $this->name.'::'.$resource;
    }

    public function view($view, $data = [], $mergeData = [])
    {
        return $this->app['view']->make($this->viewname($view), $data, $mergeData);
    }

    public function specName($resource)
    {
        return $this->name.'::'.$resource;
    }

    public function spec($path)
    {
        return $this->app[SpecFactory::class]->make($this->specName($path));
    }

}