<?php

namespace Sinta\Laravel\Addons\Repository;

use ArrayAccess;
use Illuminate\Support\NamespacedItemResolver;
use Sinta\Laravel\Addons\Contracts\LoaderInterface;

class NamespacedRepository extends NamespacedItemResolver implements ArrayAccess
{

    protected $loader;

    protected $items = [];


    public function __construct(LoaderInterface $loader)
    {
        $this->loader = $loader;
    }


    public function has($key)
    {
        $default = microtime(true);
        return $this->get($key, $default) !== $default;
    }

    public function hasGroup($key)
    {
        list($namespace, $group, $item) = $this->parseKey($key);
        return $this->loader->exists($group, $namespace);
    }

    public function get($key, $default = null)
    {
        list($namespace, $group, $item) = $this->parseKey($key);
        $collection = $this->getCollection($group, $namespace);
        $this->load($group, $namespace, $collection);
        return array_get($this->items[$collection], $item, $default);
    }

    public function set($key, $value)
    {
        list($namespace, $group, $item) = $this->parseKey($key);
        $collection = $this->getCollection($group, $namespace);

        $this->load($group, $namespace, $collection);
        if (is_null($item)) {
            $this->items[$collection] = $value;
        } else {
            array_set($this->items[$collection], $item, $value);
        }
    }

    protected function load($group, $namespace, $collection)
    {

        if (isset($this->items[$collection])) {
            return;
        }
        $items = $this->loader->load($group, $namespace);
        $this->items[$collection] = $items;
    }

    protected function getPackageNamespace($package, $namespace)
    {
        if (is_null($namespace)) {
            list($vendor, $namespace) = explode('/', $package);
        }
        return $namespace;
    }

    protected function getCollection($group, $namespace = null)
    {
        $namespace = $namespace ?: '*';
        return $namespace.'::'.$group;
    }

    public function addNamespace($namespace, $hint)
    {
        $this->loader->addNamespace($namespace, $hint);
    }


    public function getNamespaces()
    {
        return $this->loader->getNamespaces();
    }

    public function getLoader()
    {
        return $this->loader;
    }

    public function setLoader(LoaderInterface $loader)
    {
        $this->loader = $loader;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function offsetExists($key)
    {
        return $this->has($key);
    }


    public function offsetGet($key)
    {
        return $this->get($key);
    }
    /**
     * Set a configuration option.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function offsetSet($key, $value)
    {
        $this->set($key, $value);
    }
    /**
     * Unset a configuration option.
     *
     * @param string $key
     */
    public function offsetUnset($key)
    {
        $this->set($key, null);
    }
}