<?php

namespace Sinta\Laravel\Addons;

use Illuminate\Support\AggregateServiceProvider;
use Illuminate\Foundation\Providers\ComposerServiceProvider;


class ServiceProvider extends AggregateServiceProvider
{

    protected $providers = [
        Providers\ArtisanServiceProvider::class,
        Providers\ExtensionServiceProvider::class,
        ComposerServiceProvider::class,
    ];
}