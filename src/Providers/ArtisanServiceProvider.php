<?php

namespace Sinta\Laravel\Addons\Providers;

use Sinta\Laravel\Addons\Commands;
use Sinta\Laravel\Addons\ServiceProvider;

class ArtisanServiceProvider extends ServiceProvider
{
    protected $defer = true;

    protected $commands = [

    ];

    protected $devCommands = [
        'MakeAddon' => 'command+.addon.make',
    ];


    public function register()
    {
        $this->registerCommands($this->availableCommands());
    }


    protected function registerCommands(array $commands)
    {
        foreach ($commands as $name => $command) {
            $this->{"register{$name}Command"}($command);
        }

        $this->commands(array_values($commands));
    }

    protected function availableCommands()
    {
        $commands = $this->commands;

        if ($this->app->environment() != 'production') {
            $commands = array_merge($commands, $this->devCommands);
        }

        return $commands;
    }

    protected function registerAddonListCommand($command)
    {
        $this->app->singleton($command, function ($app) {
            return new Commands\AddonListCommand($app);
        });
    }

    protected function registerMakeAddonCommand($command)
    {
        $this->app->singleton($command,function($app){
            return new Commands\AddonMakeCommand($app);
        });
    }
}